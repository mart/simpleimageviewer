import Qt 4.7

Rectangle {
    width: 360
    height: 360
    Flickable {
        anchors.fill:  parent
        contentWidth: mainImage.width
        contentHeight: mainImage.height
        interactive:  true
        Image {
            id:mainImage
            objectName: "mainImage"
        }
    }
}
