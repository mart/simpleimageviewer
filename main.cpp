#include <QtGui/QApplication>
#include "qmlapplicationviewer.h"
#include <QDeclarativeItem>
#include <QtDBus/QtDBus>

#include "activitymanager_interface.h"

int main(int argc, char *argv[])
{
    QApplication app(argc, argv);

    QmlApplicationViewer viewer;
    viewer.setOrientation(QmlApplicationViewer::ScreenOrientationAuto);
    viewer.setMainQmlFile(QLatin1String("qml/simpleimageviewer/main.qml"));
    viewer.showExpanded();

    QDeclarativeItem *image = viewer.rootObject()->findChild<QDeclarativeItem*>("mainImage");
    image->setProperty("source", app.arguments().at(1));

    if (!QDBusConnection::sessionBus().isConnected()) {
        qDebug() << "Cannot connect to the D-Bus session bus.\n"
                 << "To start it, run:\n"
                 << "\teval `dbus-launch --auto-syntax`\n";
        return 1;
    }

    OrgKdeActivityManagerInterface iface("org.kde.kactivitymanagerd", "/ActivityManager", QDBusConnection::sessionBus());
    if (iface.isValid()) {
        QDBusReply<QString> reply = iface.call("NotifyResourceOpened", "simpleimageviewer", (uint)viewer.winId(), app.arguments().at(1));
        if (reply.isValid()) {
            qDebug() << "Reply was:" << qPrintable(reply.value());
        } else {
            qDebug() << "Call failed:" << qPrintable(reply.error().message());
            //return 1;
        }
        reply = iface.call("NotifyResourceAccessed", "simpleimageviewer", app.arguments().at(1));
        if (reply.isValid()) {
            qDebug() << "Reply was:" << qPrintable(reply.value());
        } else {
            qDebug() << "Call failed:" << qPrintable(reply.error().message());
            //return 1;
        }

    }

    bool result = app.exec();

    QDBusReply<QString> reply = iface.call("NotifyResourceClosed", (uint)viewer.winId(), app.arguments().at(1));
    if (reply.isValid()) {
        qDebug() << "Reply was:" << qPrintable(reply.value());
    }

    return result;
}
